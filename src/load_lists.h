
#include "world.h"

void zero_item_entry (item_struct *item);
void reset_world_info (world_info *info);
void set_window (world_info *info);
void load_settings (world_info *info);

