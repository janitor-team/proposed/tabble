
#include <gtk/gtk.h>

#include "world.h"

void show_edit_buttons (world_info *info, gboolean show_buttons);
gboolean click_leave (GtkWidget *w, GdkEventButton *ev, cell_struct *c);
gboolean click_enter (GtkWidget *w, GdkEventButton *ev, cell_struct *c);
gboolean click_press (GtkWidget *w, GdkEventButton *ev, cell_struct *c);
gboolean click_release (GtkWidget *w, GdkEventButton *ev, cell_struct *c);
gboolean key_activate (GtkWidget *w, GdkEventButton *ev, cell_struct *c);
void choices (world_info *info);

