
#include <string.h>
#include <stdlib.h>

#include <gtk/gtk.h>

#include "world.h"
#include "add_stuff.h"
#include "edits.h"
#include "load_lists.h"
#include "save_lists.h"
#include "icon.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


void cancel (GtkWidget *widget, GtkWidget *widget_to_close)
{
  gtk_widget_destroy (widget_to_close);
}


void accept_new_tab (GtkWidget *widget, world_info *info)
{
  const gchar *text;
  
  text = gtk_entry_get_text (GTK_ENTRY (info->options_field_1));
  if ((text == NULL) || (strcmp (text, "") == 0))
    return;
  add_tab (info, text);
  gtk_widget_destroy (info->options_dialog);
  gtk_notebook_set_current_page (GTK_NOTEBOOK (info->tabs), -1);
  save_all (info);
}


void new_tab (GtkWidget *widget, world_info *info)
{
  GtkWidget *label, *button;

  gtk_widget_destroy (info->options_dialog);
  info->options_dialog = gtk_dialog_new_with_buttons ("New Tab",
    GTK_WINDOW (info->main_window), GTK_DIALOG_NO_SEPARATOR, NULL);
  gtk_window_set_transient_for (GTK_WINDOW (info->options_dialog),
    GTK_WINDOW (info->main_window));
  gtk_window_set_modal (GTK_WINDOW (info->options_dialog), TRUE);
  gtk_container_set_border_width (GTK_CONTAINER (info->options_dialog),
    info->frame_spacing);
  info->options_table = gtk_table_new (3, 5, FALSE);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (info->options_dialog)->vbox),
    info->options_table);
  gtk_table_set_row_spacings (GTK_TABLE (info->options_table),
    info->frame_spacing);
  gtk_table_set_col_spacings (GTK_TABLE (info->options_table),
    info->frame_spacing);

  label = gtk_label_new ("New tab name:");
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), label,
    0, 1, 0, 1);

  info->options_field_1 = gtk_entry_new();
  gtk_table_attach_defaults (GTK_TABLE (info->options_table),
    info->options_field_1, 2, 3, 0, 1);
  
  button = gtk_button_new_with_label ("OK");
  g_signal_connect (G_OBJECT (button), "clicked",
    G_CALLBACK (accept_new_tab), info);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), button,
    4, 5, 0, 1);
  
  button = gtk_button_new_with_label ("Cancel");
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (cancel),
    info->options_dialog);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), button,
    4, 5, 2, 3);
  
  gtk_widget_show_all (info->options_dialog);
}


void accept_mod_tab (GtkWidget *widget, world_info *info)
{
  GtkWidget *table, *label;
  const gchar *text;
  gint tab_number;
  tab_struct *tab;

  tab_number = gtk_notebook_get_current_page (GTK_NOTEBOOK (info->tabs));
  text = gtk_entry_get_text (GTK_ENTRY (info->options_field_1));
  if ((text == NULL) || (strcmp (text, "") == 0))
    del_tab (info, tab_number);
  else
  {
    table = gtk_notebook_get_nth_page (GTK_NOTEBOOK (info->tabs), tab_number);
    label = gtk_notebook_get_tab_label (GTK_NOTEBOOK (info->tabs), table);
    gtk_label_set_text (GTK_LABEL (label), text);
    tab = (tab_struct*) g_list_nth (info->tab_list, tab_number)->data;
    g_string_assign (tab->tab_name, text);
  }
  gtk_widget_destroy (info->options_dialog);
  save_all (info);
}


void mod_tab (GtkWidget *widget, world_info *info)
{
  GtkWidget *label, *button;
  tab_struct *tab;
  gint tab_number;

  gtk_widget_destroy (info->options_dialog);
  info->options_dialog = gtk_dialog_new_with_buttons ("Modify Tab",
    GTK_WINDOW (info->main_window), GTK_DIALOG_NO_SEPARATOR, NULL);
  gtk_window_set_transient_for (GTK_WINDOW (info->options_dialog),
    GTK_WINDOW (info->main_window));
  gtk_window_set_modal (GTK_WINDOW (info->options_dialog), TRUE);
  gtk_container_set_border_width (GTK_CONTAINER (info->options_dialog),
    info->frame_spacing);
  info->options_table = gtk_table_new (4, 5, FALSE);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (info->options_dialog)->vbox),
    info->options_table);
  gtk_table_set_row_spacings (GTK_TABLE (info->options_table), info->frame_spacing);
  gtk_table_set_col_spacings (GTK_TABLE (info->options_table), info->frame_spacing);

  label = gtk_label_new ("New name:");
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), label,
    0, 1, 0, 1);

  info->options_field_1 = gtk_entry_new();
  tab_number = gtk_notebook_get_current_page (GTK_NOTEBOOK (info->tabs));
  tab = (tab_struct*) g_list_nth (info->tab_list, tab_number)->data;
  gtk_entry_set_text (GTK_ENTRY (info->options_field_1), tab->tab_name->str);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table),
    info->options_field_1, 2, 3, 0, 1);
  
  button = gtk_button_new_with_label ("OK");
  g_signal_connect (G_OBJECT (button), "clicked",
    G_CALLBACK (accept_mod_tab), info);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), button,
    4, 5, 0, 1);
  
  button = gtk_button_new_with_label ("Cancel");
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (cancel),
    info->options_dialog);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), button,
    4, 5, 1, 2);
  
  label = gtk_label_new ("Remove name to delete tab.");
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), label,
    0, 3, 2, 3);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);

  gtk_widget_show_all (info->options_dialog);
}


void delete_stuff (GtkWidget *widget, world_info *info)
{
  GString *message;
  
  gtk_widget_destroy (info->options_dialog);
  message = g_string_new (NULL);
  g_string_sprintf (message,
    "All delete features are not coded, but you may\n"
    "close and edit the text configuration file.\n"
    "Tabble is currently using this location:\n\n%s\n\n"
    "(Path may be relative to current working directory.)",
    info->conf_file->str);
  show_notice (message->str, info->main_window);
}


void dimensions_accept (GtkWidget *widget, world_info *info)
{
  gint x, y, t;
  
  x = atoi (gtk_entry_get_text (GTK_ENTRY (info->options_field_1)));
  y = atoi (gtk_entry_get_text (GTK_ENTRY (info->options_field_2)));

  if (x <=0 || y <= 0)
  {
    gtk_widget_destroy (info->options_dialog);
    return;
  }

  info->x_icons_new = x;
  info->y_icons_new = y;
  save_all_compressed (info);

  for (t = info->tab_count - 1; t >= 0; t--)
    del_tab (info, t);
  info->run_again = TRUE;
  gtk_widget_destroy (info->options_dialog);
  gtk_widget_destroy (info->main_window);
}


void dimensions (GtkWidget *widget, world_info *info)
{
  GtkWidget *label, *button;
  GString *string;

  gtk_widget_destroy (info->options_dialog);
  info->options_dialog = gtk_dialog_new_with_buttons ("Dimensions",
    GTK_WINDOW (info->main_window), GTK_DIALOG_NO_SEPARATOR, NULL);
  gtk_window_set_transient_for (GTK_WINDOW (info->options_dialog),
    GTK_WINDOW (info->main_window));
  gtk_window_set_modal (GTK_WINDOW (info->options_dialog), TRUE);
  gtk_container_set_border_width (GTK_CONTAINER (info->options_dialog),
    info->frame_spacing);
  info->options_table = gtk_table_new (4, 3, FALSE);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (info->options_dialog)
    ->vbox), info->options_table);
  gtk_table_set_row_spacings (GTK_TABLE (info->options_table),
    info->frame_spacing);
  gtk_table_set_col_spacings (GTK_TABLE (info->options_table),
    info->frame_spacing);

  label = gtk_label_new ("X across:");
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), label,
    0, 1, 0, 1);
  
  label = gtk_label_new ("Y down:");
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), label,
    0, 1, 1, 2);

  string = g_string_new (NULL);
    
  info->options_field_1 = gtk_entry_new();
  g_string_sprintf (string, "%d", info->x_icons);
  gtk_entry_set_text (GTK_ENTRY (info->options_field_1), string->str);
  gtk_table_attach (GTK_TABLE (info->options_table), info->options_field_1,
    1, 2, 0, 1, GTK_SHRINK, GTK_SHRINK, 0, 0);

  info->options_field_2 = gtk_entry_new();
  g_string_sprintf (string, "%d", info->y_icons);
  gtk_entry_set_text (GTK_ENTRY (info->options_field_2), string->str);
  gtk_table_attach (GTK_TABLE (info->options_table), info->options_field_2,
    1, 2, 1, 2, GTK_SHRINK, GTK_SHRINK, 0, 0);

  button = gtk_button_new_with_label ("OK");
  gtk_table_attach (GTK_TABLE (info->options_table), button, 2, 3, 0, 1,
    GTK_FILL, GTK_SHRINK, 0, 0);
  g_signal_connect (G_OBJECT (button), "clicked",
    G_CALLBACK (dimensions_accept), info);
  
  button = gtk_button_new_with_label ("Cancel");
  gtk_table_attach (GTK_TABLE (info->options_table), button, 2, 3, 1, 2,
    GTK_FILL, GTK_SHRINK, 0, 0);
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (cancel),
    info->options_dialog);

  label = gtk_label_new ("NB: clicking OK will compress your\n"
    "programs linearily.");
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), label,
    0, 3, 2, 3);

  gtk_widget_show_all (info->options_dialog);
}


void position_custom (GtkWidget *widget, world_info *info)
{
  gtk_widget_destroy (info->options_dialog);
  gtk_window_get_position (GTK_WINDOW (info->main_window),
    &info->start_x, &info->start_y);
  gtk_window_get_size (GTK_WINDOW (info->main_window),
    &info->start_w, &info->start_h);
  save_all (info);
}


void position_window_manager (GtkWidget *widget, world_info *info)
{
  gtk_widget_destroy (info->options_dialog);
  info->start_x = info->start_y = -1;
  info->start_w = info->start_h = -1;
  save_all (info);
}


void position_options (GtkWidget *widget, world_info *info)
{
  GtkWidget *button;

  gtk_widget_destroy (info->options_dialog);
  info->options_dialog = gtk_dialog_new_with_buttons ("Window Position",
    GTK_WINDOW (info->main_window), GTK_DIALOG_NO_SEPARATOR, NULL);
  gtk_window_set_transient_for (GTK_WINDOW (info->options_dialog),
    GTK_WINDOW (info->main_window));
  gtk_window_set_modal (GTK_WINDOW (info->options_dialog), TRUE);
  gtk_container_set_border_width (GTK_CONTAINER (info->options_dialog),
    info->frame_spacing);
  info->options_table = gtk_table_new (3, 2, FALSE);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (info->options_dialog)->vbox),
    info->options_table);
  gtk_table_set_row_spacings (GTK_TABLE (info->options_table),
    info->frame_spacing);
  gtk_table_set_col_spacings (GTK_TABLE (info->options_table),
    info->frame_spacing);

  button = gtk_button_new_with_label ("Save current screen position");
  g_signal_connect (G_OBJECT (button), "clicked",
    G_CALLBACK (position_custom), info);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), button,
    0, 1, 0, 1);
  
  button = gtk_button_new_with_label ("Window manager decided");
  g_signal_connect (G_OBJECT (button), "clicked",
    G_CALLBACK (position_window_manager), info);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), button,
    0, 1, 1, 2);
    
  button = gtk_button_new_with_label ("Cancel");
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (cancel),
    info->options_dialog);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), button,
    1, 2, 2, 3);
  
  gtk_widget_show_all (info->options_dialog);
}


void accept_ontop_sticky (GtkWidget *widget, world_info *info)
{
  info->world_on_top =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (info->options_check_1));
  info->world_sticky =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (info->options_check_2));
  info->world_revert_to_first =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (info->options_check_3));

  gtk_widget_destroy (info->options_dialog);
  set_window (info);
  save_all (info);
}


void behaviour (GtkWidget *widget, world_info *info)
{
  GtkWidget *button;

  gtk_widget_destroy (info->options_dialog);
  info->options_dialog = gtk_dialog_new_with_buttons ("Tabble Behaviour",
    GTK_WINDOW (info->main_window), GTK_DIALOG_NO_SEPARATOR, NULL);
  gtk_window_set_transient_for (GTK_WINDOW (info->options_dialog),
    GTK_WINDOW (info->main_window));
  gtk_window_set_modal (GTK_WINDOW (info->options_dialog), TRUE);
  gtk_container_set_border_width (GTK_CONTAINER (info->options_dialog),
    info->frame_spacing);
  info->options_table = gtk_table_new (3, 3, FALSE);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (info->options_dialog)
    ->vbox), info->options_table);
  gtk_table_set_row_spacings (GTK_TABLE (info->options_table),
    info->frame_spacing);
  gtk_table_set_col_spacings (GTK_TABLE (info->options_table),
    info->frame_spacing);

  info->options_check_1 = gtk_check_button_new_with_label ("Stay on top");
  gtk_table_attach_defaults (GTK_TABLE (info->options_table),
    info->options_check_1, 0, 1, 0, 1);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (info->options_check_1),
    info->world_on_top);

  info->options_check_2 = gtk_check_button_new_with_label ("All desktops");
  gtk_table_attach_defaults (GTK_TABLE (info->options_table),
    info->options_check_2, 0, 1, 1, 2);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (info->options_check_2),
    info->world_sticky);
    
  info->options_check_3 = gtk_check_button_new_with_label ("Revert to first");
  gtk_table_attach_defaults (GTK_TABLE (info->options_table),
    info->options_check_3, 0, 1, 2, 3);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (info->options_check_3),
    info->world_revert_to_first);
    
  button = gtk_button_new_with_label ("OK");
  g_signal_connect (G_OBJECT (button), "clicked",
    G_CALLBACK (accept_ontop_sticky), info);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), button,
    2, 3, 0, 1);
  
  button = gtk_button_new_with_label ("Cancel");
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (cancel),
    info->options_dialog);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), button,
    2, 3, 2, 3);
  
  gtk_widget_show_all (info->options_dialog);
}


void help (GtkWidget *widget, world_info *info)
{
  GtkWidget *dialog, *table, *label, *field, *button;

  gtk_widget_destroy (info->options_dialog);
  
  dialog = gtk_dialog_new_with_buttons ("Help",
    GTK_WINDOW (info->main_window), GTK_DIALOG_NO_SEPARATOR, NULL);
  gtk_window_set_transient_for (GTK_WINDOW (dialog),
    GTK_WINDOW (info->main_window));
  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_container_set_border_width (GTK_CONTAINER (dialog),
    info->frame_spacing);
  table = gtk_table_new (5, 4, FALSE);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), table);
  gtk_table_set_row_spacings (GTK_TABLE (table), info->frame_spacing);
  gtk_table_set_col_spacings (GTK_TABLE (table), info->frame_spacing);

  label = gtk_label_new ("\nBecause of size constraints this is basic tabble\n"
    "help, for more please visit:\n");
  gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 3, 0, 1);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);

  field = gtk_entry_new();
  gtk_entry_set_text (GTK_ENTRY (field),
    "http://www.rillion.net/tabble/help.html");
  gtk_entry_set_editable (GTK_ENTRY (field), FALSE);
  gtk_table_attach_defaults (GTK_TABLE (table), field, 0, 3, 1, 2);

  label = gtk_label_new
  (
    "\nThe buttons of programs appear on their names\n"
    "with mouse over. When visible a button may be\n"
    "left clicked to be launched, right clicked to be\n"
    "edited and middle clicked to be launched with\n"
    "auto-hide.\n\n"
    "To make the buttons of empty cells visible press\n"
    "shift. Programs may also be copied around with\n"
    "a right click."
  );
  gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 3, 2, 3);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);

  button = gtk_button_new_with_label ("OK");
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (dismiss_notice),
    dialog);
  gtk_table_attach (GTK_TABLE (table), button, 3, 4, 3, 4,
    GTK_SHRINK, GTK_SHRINK, 0, 0);

  gtk_widget_show_all (dialog);
}


void about (GtkWidget *widget, world_info *info)
{
  GtkWidget *dialog, *table, *icon, *label, *field, *button;
  GdkPixbuf *pix_buf;
  GString *string;

  gtk_widget_destroy (info->options_dialog);
  
  dialog = gtk_dialog_new_with_buttons ("About",
    GTK_WINDOW (info->main_window), GTK_DIALOG_NO_SEPARATOR, NULL);
  gtk_window_set_transient_for (GTK_WINDOW (dialog),
    GTK_WINDOW (info->main_window));
  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_container_set_border_width (GTK_CONTAINER (dialog),
    info->frame_spacing);
  table = gtk_table_new (6, 5, FALSE);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), table);
  gtk_table_set_row_spacings (GTK_TABLE (table), info->frame_spacing);
  gtk_table_set_col_spacings (GTK_TABLE (table), info->frame_spacing);

  pix_buf = gdk_pixbuf_new_from_xpm_data (tabble_icon());
  if (pix_buf)
  {
    icon = gtk_image_new_from_pixbuf (pix_buf);
    if (icon)
    {
      gtk_table_attach (GTK_TABLE (table), icon,
       0, 1, 2, 3, GTK_SHRINK, GTK_SHRINK, info->frame_spacing, 0);
    }
  }

  string = g_string_new (NULL);
  g_string_sprintf (string, "%s %s\n\n"
    "Distributed under the GPL2 this program is copyright\n"
    "2005, 2006, 2010 Graeme Sheppard.\n\n"
    "Homepage:\n", PACKAGE, VERSION);
  
  label = gtk_label_new (string->str);
  gtk_table_attach_defaults (GTK_TABLE (table), label, 2, 3, 2, 3);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);

  field = gtk_entry_new();
  gtk_entry_set_text (GTK_ENTRY (field), "http://www.rillion.net/tabble/");
  gtk_entry_set_editable (GTK_ENTRY (field), FALSE);
  gtk_table_attach_defaults (GTK_TABLE (table), field, 2, 3, 3, 4);

  button = gtk_button_new_with_label ("OK");
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (dismiss_notice),
    dialog);
  gtk_table_attach (GTK_TABLE (table), button, 4, 5, 5, 6,
    GTK_SHRINK, GTK_SHRINK, 0, 0);

  gtk_widget_show_all (dialog);
}


void add (world_info *info, gint row, gchar *button_text, gchar *label_text,
          GCallback function)
{
  GtkWidget *button, *label;
  
  button = gtk_button_new_with_label (button_text);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), button,
    0, 1, row, row + 1);
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (function), info);
  
  label = gtk_label_new (label_text);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), label,
    2, 3, row, row + 1);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
}


void options_dialog (world_info *info)
{
  GtkWidget *button;

  info->options_dialog = gtk_dialog_new_with_buttons ("Options",
    GTK_WINDOW (info->main_window), GTK_DIALOG_NO_SEPARATOR, NULL);
  gtk_window_set_transient_for (GTK_WINDOW (info->options_dialog),
    GTK_WINDOW (info->main_window));
  gtk_window_set_modal (GTK_WINDOW (info->options_dialog), TRUE);
  gtk_container_set_border_width (GTK_CONTAINER (info->options_dialog),
    info->frame_spacing);
  info->options_table = gtk_table_new (10, 4, FALSE);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (info->options_dialog)->vbox),
    info->options_table);
  gtk_table_set_row_spacings (GTK_TABLE (info->options_table),
    info->frame_spacing);
  gtk_table_set_col_spacings (GTK_TABLE (info->options_table),
    info->frame_spacing * 2);

  add (info, 0, "Help", "Quick tips",
    G_CALLBACK (help));
  add (info, 1, "New Tab", "Create a new, empty tab",
    G_CALLBACK (new_tab));
  add (info, 2, "Modify tab", "Rename or delete current tab",
    G_CALLBACK (mod_tab));
  add (info, 3, "Delete", "How to remove other objects",
    G_CALLBACK (delete_stuff));
  add (info, 4, "Dimensions", "Set rows and columns",
    G_CALLBACK (dimensions));
  add (info, 5, "Position", "Screen position",
    G_CALLBACK (position_options));
  add (info, 6, "Behaviour", "On top, sticky, unhide options",
    G_CALLBACK (behaviour));
  add (info, 7, "About", "",
    G_CALLBACK (about));

  button = gtk_button_new_with_label ("Cancel");
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (cancel),
    info->options_dialog);
  gtk_table_attach_defaults (GTK_TABLE (info->options_table), button,
    3, 4, 9, 10);
  
  gtk_widget_show_all (info->options_dialog);
}

