
#include <gtk/gtk.h>

#include "world.h"
#include "choices.h"
#include "load_lists.h"
#include "signals.h"


int main (int argc, char *argv[])
{
  GtkWidget *main_window;
  world_info *info;
  gboolean run_again;
  
  gtk_init (&argc, &argv);
  while (TRUE)
  {
    main_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    info = g_malloc0 (sizeof (world_info));
    info->argc = argc;
    info->argv = argv;
    info->main_window = main_window;
    reset_world_info (info);
    choices (info);
    gtk_window_set_title (GTK_WINDOW (main_window), info->window_name->str);
    gtk_main();
    signal_init (NULL);
    run_again = info->run_again;
    g_free (info);
    if (run_again == FALSE)
      return 0;
  }
}

