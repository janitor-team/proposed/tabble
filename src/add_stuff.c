
#include <string.h>

#include <gtk/gtk.h>

#include "world.h"
#include "choices.h"
#include "read_line.h"
#include "icon.h"
#include "icons.h"


static const gchar blank_spaces[] = "       "; /* for a blank button */


const gchar* fragment (world_info *info, const gchar *text)
{
  gchar *output;
  gint max_line, size, index, base, last_space;
  
  output = g_string_new (text)->str;
  max_line = info->max_line;
  size = strlen (text);
  base = 0;
  last_space = -1;
  
  for (index = 0; index < size; index++)
  {
    if (output [index] == ' ')
    {
      if (index - base > max_line)
      {
        if (last_space > base)
        {
          output [last_space] = '\n';
          base = last_space;
        }
        else
        {
          output [index] = '\n';
          base = index;
        }
      }
      last_space = index;
    }
  }
  
  return output;
}


void zero_item (item_struct *item)
{
  item->name = g_string_new (NULL);
  item->dir = g_string_new (NULL);
  item->cmd = g_string_new (NULL);
  item->icon = g_string_new (NULL);
  item->x_pos = item->y_pos = -1;
}


void free_item (item_struct *item)
{
  g_string_free (item->name, TRUE);
  g_string_free (item->dir, TRUE);
  g_string_free (item->cmd, TRUE);
  g_string_free (item->icon, TRUE);
}


void copy_item (const item_struct *src, item_struct *dest)
{
  g_string_assign (dest->name, src->name->str);
  g_string_assign (dest->dir, src->dir->str);
  g_string_assign (dest->cmd, src->cmd->str);
  g_string_assign (dest->icon, src->icon->str);
}


cell_struct* get_grid (world_info *info, gint tab_number, gint x, gint y)
{
  tab_struct *tab;
  cell_struct *cells;
 
  tab = (tab_struct*) g_list_nth (info->tab_list, tab_number)->data;
  cells = (cell_struct*) tab->cell_array;
  return &cells [y * info->x_icons + x];
}


void setup_cell (world_info *info, gint tab_number, gint x, gint y)
{
  item_struct *item;
  cell_struct *cell;
 
  cell = get_grid (info, tab_number, x, y);
  cell->info = (struct world_info*) info;
  cell->tab_pos = tab_number;
  cell->filled = FALSE;
  cell->icon_image = cell->button = cell->label = NULL;

  item = g_malloc0 (sizeof (item_struct));
  item->name = g_string_new (NULL);
  item->dir = g_string_new (NULL);
  item->cmd = g_string_new (NULL);
  item->icon = g_string_new (NULL);
  item->x_pos = x;
  item->y_pos = y;
  cell->item = item;
}


void connect_cell (cell_struct *cell)
{
  g_signal_connect (G_OBJECT (cell->button), "activate",
    G_CALLBACK (key_activate), cell); 
  g_signal_connect (G_OBJECT (cell->button), "button-press-event",
    G_CALLBACK (click_press), cell); 
  g_signal_connect (G_OBJECT (cell->button), "button-release-event",
    G_CALLBACK (click_release), cell);
  g_signal_connect (G_OBJECT (cell->button), "enter-notify-event",
    G_CALLBACK (click_enter), cell);
  g_signal_connect (G_OBJECT (cell->button), "leave-notify-event",
    G_CALLBACK (click_leave), cell);

  if (cell->icon_button)
  {
    g_signal_connect (G_OBJECT (cell->icon_button), "activate",
      G_CALLBACK (key_activate), cell);
    g_signal_connect (G_OBJECT (cell->icon_button), "button-press-event",
      G_CALLBACK (click_press), cell); 
    g_signal_connect (G_OBJECT (cell->icon_button), "button-release-event",
      G_CALLBACK (click_release), cell);
    g_signal_connect (G_OBJECT (cell->icon_button), "enter-notify-event",
      G_CALLBACK (click_enter), cell);
    g_signal_connect (G_OBJECT (cell->icon_button), "leave-notify-event",
      G_CALLBACK (click_leave), cell);
  }    
}


void hide_cell_visibles (cell_struct *cell)
{
  if (cell->icon_image)
    gtk_widget_destroy (cell->icon_image);
  if (cell->button)
    gtk_widget_destroy (cell->button);
  if (cell->icon_button)
    gtk_widget_destroy (cell->icon_button);
  if (cell->label)
    gtk_widget_destroy (cell->label);
  cell->icon_image = cell->button = cell->icon_button = cell->label = NULL;
}


void show_empty_cell (world_info *info, gint tab_number, gint x, gint y)
{
  GdkPixbuf *pix_buf;
  tab_struct *tab;
  cell_struct *cell;
 
  tab = (tab_struct*) g_list_nth (info->tab_list, tab_number)->data;
  cell = get_grid (info, tab_number, x, y);
  hide_cell_visibles (cell);  
  pix_buf = gdk_pixbuf_new_from_xpm_data (blank_icon());
  if (pix_buf)
  {
    cell->icon_image = gtk_image_new_from_pixbuf (pix_buf);
    if (cell->icon_image)
    {
      gtk_table_attach (GTK_TABLE (tab->box_widget), cell->icon_image,
        x, x+1, y*3, y*3+1, GTK_FILL, GTK_FILL,
        0, 0);
      gtk_widget_show (cell->icon_image);
    }
    g_object_unref (pix_buf);
  }
  cell->button = gtk_button_new_with_label (blank_spaces);
  gtk_table_attach (GTK_TABLE (tab->box_widget), cell->button,
    x, x+1, y*3+1, y*3+2, GTK_SHRINK, GTK_SHRINK,
    0, 0);
  gtk_widget_hide (cell->button); /* yes a hide */
  /* if the above is made show, widget sizes become correct */
}

 
void push_blank_cell (world_info *info, gint tab_number, gint x, gint y)
{
  GdkPixbuf *pix_buf;
  cell_struct *cell;
  tab_struct *tab;
 
  tab = (tab_struct*) g_list_nth (info->tab_list, tab_number)->data;
  cell = get_grid (info, tab_number, x, y);
  hide_cell_visibles (cell);  
  pix_buf = gdk_pixbuf_new_from_xpm_data (blank_icon());
  if (pix_buf)
  {
    cell->icon_image = gtk_image_new_from_pixbuf (pix_buf);
    if (cell->icon_image)
    {
      gtk_table_attach (GTK_TABLE (tab->box_widget), cell->icon_image,
        x, x+1, y*3, y*3+1, GTK_FILL, GTK_FILL,
        0, 0);
      gtk_widget_show (cell->icon_image);
    }
    g_object_unref (pix_buf);
  }
  cell->button = gtk_button_new_with_label (blank_spaces);
  gtk_table_attach (GTK_TABLE (tab->box_widget), cell->button,
    x, x+1, y*3+1, y*3+2, GTK_SHRINK, GTK_SHRINK,
    0, 0);
  gtk_widget_show (cell->button);
  cell->icon_image = NULL;
  connect_cell (cell);
}


cell_struct* first_blank (world_info *info, gint tab_number)
{
  cell_struct *cell;
  gint x, y;
  
  for (y = 0; y < info->y_icons; y++)
    for (x = 0; x < info->x_icons; x++)
    {
      cell = get_grid (info, tab_number, x, y);
      if (!cell->filled)
        return cell;
    }
  return NULL; 
}


gint add_tab (world_info *info, const gchar *name)
{
  GtkWidget *label, *table;
  tab_struct *tab;
  gint tab_number, x, y;
  
  label = gtk_label_new (name);
  gtk_widget_show (label);
  table = gtk_table_new (info->y_icons * 3, info->x_icons, FALSE);
  gtk_widget_show (table);
  gtk_container_set_border_width (GTK_CONTAINER (table), info->frame_spacing);
  gtk_table_set_row_spacings (GTK_TABLE (table), info->icon_y_spacing);
  gtk_table_set_col_spacings (GTK_TABLE (table), info->icon_x_spacing);

  gtk_notebook_append_page (GTK_NOTEBOOK (info->tabs), table, label);
  tab_number = info->tab_count++; /* note order */
  
  tab = g_malloc0 (sizeof (tab_struct));
  tab->tab_name = g_string_new (name);
  tab->box_widget = table;
  info->tab_list = g_list_append (info->tab_list, tab);
  
  tab->cell_array = g_malloc0
    (sizeof (cell_struct) * info->x_icons * info->y_icons);
  for (y = 0; y < info->y_icons; y++)
  {
    for (x = 0; x < info->x_icons; x++)
    {
      setup_cell (info, tab_number, x, y);
      show_empty_cell (info, tab_number, x, y);

      label = gtk_label_new (" ");
      gtk_table_attach_defaults (GTK_TABLE (table), label,
        x, x+1, y*3+2, y*3+3);
      gtk_widget_show (label);
    }
  }
  
  return tab_number;
}


void del_tab (world_info *info, gint tab_number)
{
  cell_struct *cell;
  tab_struct *tab;
  gint x, y;

  for (y = 0; y < info->y_icons; y++)
    for (x = 0; x < info->x_icons; x++)
    {
      cell = get_grid (info, tab_number, x, y);
      hide_cell_visibles (cell);
      free_item (cell->item);
    }

  gtk_notebook_remove_page (GTK_NOTEBOOK (info->tabs), tab_number);

  tab = (tab_struct*) g_list_nth (info->tab_list, tab_number)->data;
  g_string_free (tab->tab_name, TRUE);
  g_free (tab->cell_array);
  g_free (tab);

  info->tab_list = g_list_remove (info->tab_list,
    (tab_struct*) g_list_nth (info->tab_list, tab_number)->data);
  info->tab_count--;
  if (info->tab_count > 0)
  {
    if (tab_number >= info->tab_count)
      tab_number = info->tab_count - 1;
    gtk_notebook_set_current_page (GTK_NOTEBOOK (info->tabs), tab_number);
  }
}


void pinup_item (cell_struct *cell)
{
  GtkWidget *label;
  GdkPixbuf *pix_buf;
  GString *icon_file;
  world_info *info;
  item_struct *item;
  tab_struct *tab;
  gint x, y;
 
  info = (world_info*) cell->info;
  item = cell->item;
  hide_cell_visibles (cell);
  tab = (tab_struct*) g_list_nth (info->tab_list, cell->tab_pos)->data;
  
  icon_file = g_string_new (NULL);
  x = item->x_pos;
  y = item->y_pos * 3;
  
  if (!text_here (item->icon))
    g_string_assign (icon_file, "");
  else
  {
    if ((item->icon->str [0] == '/') || (item->icon->str [0] == '.'))
      g_string_assign (icon_file, item->icon->str);
    else
      g_string_sprintf (icon_file, "%s/%s", info->icon_dir->str,
        item->icon->str);
  }
  cell->icon_image = scaled_icon (icon_file->str);
  if (!cell->icon_image)
  {
    pix_buf = gdk_pixbuf_new_from_xpm_data (blank_icon());
    cell->icon_image = gtk_image_new_from_pixbuf (pix_buf);
    cell->icon_button = NULL;

    gtk_table_attach (GTK_TABLE (tab->box_widget), cell->icon_image,
      x, x+1, y, y+1, GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_widget_show (cell->icon_image);
  }
  else
  {
    cell->icon_button = gtk_button_new();
    gtk_container_add (GTK_CONTAINER (cell->icon_button), cell->icon_image);
    gtk_button_set_relief (GTK_BUTTON (cell->icon_button), GTK_RELIEF_NONE);

    gtk_table_attach (GTK_TABLE (tab->box_widget), cell->icon_button,
      x, x+1, y, y+1, GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_widget_show (cell->icon_image);
    gtk_widget_show (cell->icon_button);
  }
  
  cell->button = gtk_button_new();
  if (text_here (item->name))
    label = gtk_label_new (fragment (info, item->name->str));
  else
    label = gtk_label_new ("");
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_CENTER);
  gtk_container_add (GTK_CONTAINER (cell->button), label);
  gtk_button_set_relief (GTK_BUTTON (cell->button), GTK_RELIEF_NONE);

  connect_cell (cell);

  gtk_table_attach (GTK_TABLE (tab->box_widget), cell->button,
    x, x+1, y+1, y+2, GTK_SHRINK, GTK_SHRINK, 0, 0);
  gtk_widget_show (label);
  gtk_widget_show (cell->button);
  cell->filled = TRUE;
}


void add_item (world_info *info, const item_struct *source, gint tab_number)
{
  cell_struct *cell = NULL;
  item_struct *item;
  gboolean guess;
  
  guess = FALSE;
  if (source->x_pos < 0 || source->y_pos < 0)
  {
    g_print ("error: no x y doing %s\n", source->name->str);
    guess = TRUE;
  }
  if (source->x_pos >= info->x_icons || source->y_pos >= info->y_icons)
  {
    g_print ("error: grid location out of range doing %s\n", source->name->str);
    guess = TRUE;
  }

  if (guess == FALSE)
  {
    cell = get_grid (info, tab_number, source->x_pos, source->y_pos);
    if (cell->filled)
    {
      g_print ("error: space already exists doing %s\n", source->name->str);
      guess = TRUE;
    }
  }
  
  if (guess == TRUE)
  {
    cell = first_blank (info, tab_number);
    if (cell == NULL)
    {
      g_print ("error: ran out of table spacing doing %s\n", source->name->str);
      return;
    }
  }
  
  item = cell->item;
  g_string_assign (item->name, source->name->str);
  g_string_assign (item->dir, source->dir->str);
  g_string_assign (item->cmd, source->cmd->str);
  g_string_assign (item->icon, source->icon->str);
  cell->filled = TRUE;
  pinup_item (cell);
}

