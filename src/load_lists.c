
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gtk/gtk.h>

#include "world.h"
#include "read_line.h"
#include "add_stuff.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


enum parse_mode {pm_settings, pm_programs};


gint my_atoi (const gchar *text)
{
  gint number;

  if (strcmp (text, "0") == 0)
    return 0;

  number = atoi (text);
  if (number == 0)
    g_print ("not an integer: %s\n", text);
  return number;
}


void reset_item (item_struct *item)
{
  g_string_assign (item->name, "");
  g_string_assign (item->dir, "");
  g_string_assign (item->cmd, "");
  g_string_assign (item->icon, "");
  item->x_pos = item->y_pos = -1;
}


void maybe_item_entry (world_info *info, item_struct *item, gint this_tab)
{
  GString *icon_path;
  
  if (!text_here (item->name))
    return;

  if (text_here (info->icon_dir) && text_here (item->icon)
    && (item->icon->str [0] != '/') && (item->icon->str [0] != '.'))
  {
    icon_path = g_string_new (NULL);
    g_string_sprintf (icon_path, "%s/%s", info->icon_dir->str, item->icon->str);
    g_string_assign (item->icon, icon_path->str);
    free_g_string (icon_path);
  }
  add_item (info, item, this_tab);
  reset_item (item);
}


const gchar* find_conf (world_info *info)
{
  const gchar *text;
  
  info->conf_file = g_string_new (NULL);
  info->pwd_dir = g_string_new (NULL);

  text = getenv ("PWD");
  if ((text) && (strcmp (text, "") != 0))
    g_string_assign (info->pwd_dir, text);
  
  text = cmdline_option (info, "-c");
  if (text)
  {
    if (strcmp (text, "") == 0)
      return "-c option with no filename";
    g_string_assign (info->conf_file, text);
    if (!file_readable (text))
      return "specified file not readable";
    else
      return NULL;
  }
  
  text = getenv ("HOME");
  if ((text) && (strcmp (text, "") != 0))
  {
    g_string_sprintf (info->conf_file, "%s/.tabble", text);
    if (file_readable (info->conf_file->str))
      return NULL;
    else
      return "choosing home directory for configuration";
  }
  
  g_string_assign (info->conf_file, "example-conf");
  if (file_readable (info->conf_file->str))
    return NULL;
  else
    return "can't find any configuration";
}


gboolean keyword_valid (const gchar *text)
{
  if (strcmp (text, "tab") == 0) return TRUE;
  if (strcmp (text, "name") == 0) return TRUE;
  if (strcmp (text, "dir") == 0) return TRUE;
  if (strcmp (text, "cmd") == 0) return TRUE;
  if (strcmp (text, "icon") == 0) return TRUE;
  if (strcmp (text, "x pos") == 0) return TRUE;
  if (strcmp (text, "y pos") == 0) return TRUE;
  if (strcmp (text, "icon dir") == 0) return TRUE;
  if (strcmp (text, "options") == 0) return TRUE;
  if (strcmp (text, "on top") == 0) return TRUE;
  if (strcmp (text, "sticky") == 0) return TRUE;
  if (strcmp (text, "revert") == 0) return TRUE;
  if (strcmp (text, "start x") == 0) return TRUE;
  if (strcmp (text, "start y") == 0) return TRUE;
  if (strcmp (text, "start w") == 0) return TRUE;
  if (strcmp (text, "start h") == 0) return TRUE;
  if (strcmp (text, "x icons") == 0) return TRUE;
  if (strcmp (text, "y icons") == 0) return TRUE;
  if (strcmp (text, "window name") == 0) return TRUE;
  return FALSE;
}


gboolean keyword_test (const gchar *left, const gchar* keyword)
{
  if (!keyword_valid (keyword))
  {
    g_print ("internal error: unknown keyword %s\n", keyword);
    return FALSE;
  }
  if (strcmp (left, keyword) == 0)
    return TRUE;
  else
    return FALSE;
}


gboolean true (const gchar *text)
{
  if (strcmp (text, "true") == 0) return TRUE;
  if (strcmp (text, "false") == 0) return FALSE;
  g_print ("not true or false: %s\n", text);
  return FALSE;
}


void read_settings (world_info *info, const gchar *left, const gchar *right)
{
  if (keyword_test (left, "options") && !true (right))
    info->options_enabled = FALSE;
  else
  if (keyword_test (left, "on top") && true (right))
    info->world_on_top = TRUE;
  else
  if (keyword_test (left, "sticky") && true (right))
    info->world_sticky = TRUE;
  else
  if (keyword_test (left, "revert") && true (right))
    info->world_revert_to_first = TRUE;
  else
  if (keyword_test (left, "start x"))
    info->start_x = my_atoi (right);
  else
  if (keyword_test (left, "start y"))
    info->start_y = my_atoi (right);
  else
  if (keyword_test (left, "start w"))
    info->start_w = my_atoi (right);
  else
  if (keyword_test (left, "start h"))
    info->start_h = my_atoi (right);
  else
  if (keyword_test (left, "x icons"))
    info->x_icons = my_atoi (right);
  else
  if (keyword_test (left, "y icons"))
    info->y_icons = my_atoi (right);
  else
  if (keyword_test (left, "window name"))
    g_string_assign (info->window_name, right);
}


void read_programs (world_info *info, const gchar *left, const gchar *right,
  item_struct *item, gint *this_tab)
{
  if (keyword_test (left, "tab"))
  {
    maybe_item_entry (info, item, *this_tab);
    *this_tab = add_tab (info, right);
  }
  else
  if (keyword_test (left, "name"))
  {
    maybe_item_entry (info, item, *this_tab);
    g_string_assign (item->name, right);
  }
  else
  if (keyword_test (left, "cmd"))
    g_string_assign (item->cmd, right);
  else
  if (keyword_test (left, "dir"))
    g_string_assign (item->dir, right);
  else
  if (keyword_test (left, "x pos"))
    item->x_pos = my_atoi (right);
  else
  if (keyword_test (left, "y pos"))
    item->y_pos = my_atoi (right);
  else
  if (keyword_test (left, "icon"))
    g_string_assign (item->icon, right);
  else
  if (keyword_test (left, "icon dir"))
    g_string_assign (info->icon_dir, right);
}


void parse_config (world_info *info, gint parse_setting)
{
  readline *context;
  const gchar *filename;
  gchar *left, *right;
  GString *line, *error_text;
  gint count = 0, index = 0, this_tab = -1;
  gboolean errors_present = FALSE;
  item_struct item;
    
  filename = info->conf_file->str;
  if (!filename)
  {
    if (parse_setting == pm_settings)
      g_print ("%s: can't load settings\n", PACKAGE);
    return;
  }
  
  line = g_string_new (NULL);
  error_text = g_string_new (NULL);
  context = read_start (filename, 256);
  if (parse_setting == pm_programs)
    zero_item (&item);

  for (;;)
  {
    if (text_here (error_text))
    {
      if (errors_present == FALSE)
      {
        g_print ("%s: errors in %s\n", PACKAGE, filename);
        errors_present = TRUE;
      }
      g_print ("line %d: %s\n", count, error_text->str);
      g_string_assign (error_text, "");
    }
    count++;
    if (!read_next (context, &line))
    {
      if (parse_setting == pm_programs)
        maybe_item_entry (info, &item, this_tab);
      return;
    }
    if (!text_here (line))
      continue;
    if ((line->str)[0] == '#')
      continue;
    index = char_find (line->str, ':');
    if (index < 0)
    {
      if (parse_setting == pm_settings)
        g_string_assign (error_text, "no : divider");
      continue;
    }
    if ((line->len < 3) || (index < 1))
    {
      if (parse_setting == pm_settings)
        g_string_assign (error_text, "line too short");
      continue;
    }
    (line->str) [index] = '\0';
    left = line->str;
    right = line->str + index + 1; /* yeah a bit iffy */
    while (*right == ' ')
      right++; /* same */
    if (strcmp (left, "") == 0)
    {
      if (parse_setting == pm_settings)
        g_string_assign (error_text, "no keyword");
      continue;
    }
    if (!keyword_valid (left))
    {
      if (parse_setting == pm_settings)
        g_string_sprintf (error_text, "unknown keyword: %s", left);
      continue;
    }
    if (strcmp (right, "") == 0)
    {
      if (parse_setting == pm_settings)
        g_string_assign (error_text, "no value");
      continue;
    }

    if (parse_setting == pm_settings)
      read_settings (info, left, right);
    else
    if (parse_setting == pm_programs)
      read_programs (info, left, right, &item, &this_tab);
  }
}


void set_window (world_info *info)
{
  gtk_window_set_keep_above (GTK_WINDOW (info->main_window),
    info->world_on_top);
  if (info->world_sticky)
    gtk_window_stick (GTK_WINDOW (info->main_window));
  else
    gtk_window_unstick (GTK_WINDOW (info->main_window));
  if ((info->start_x >=0) && (info->start_y >= 0))
    gtk_window_move (GTK_WINDOW (info->main_window),
      info->start_x, info->start_y);
  if ((info->start_w >=0) && (info->start_h >= 0))
    gtk_window_resize (GTK_WINDOW (info->main_window),
      info->start_w, info->start_h);
}


void reset_world_info (world_info *info)
{
  info->tab_list = NULL;
  info->tab_count = 0;
  info->button_spacing = 2;
  info->icon_x_spacing = 10;
  info->icon_y_spacing = 5;
  info->frame_spacing = 5;
  info->x_icons = 4;
  info->y_icons = 2;
  info->max_line = 10;
  info->icon_dir = g_string_new (default_icon_dir);
  info->cmd_dir = g_string_new (default_cmd_dir);
  info->world_on_top = FALSE;
  info->world_sticky = FALSE;
  info->world_revert_to_first = FALSE;
  info->start_x = info->start_y = -1;
  info->start_w = info->start_h = -1;
  info->blanks_shown = FALSE;
  info->current_cell = info->selected_cell = NULL;
  info->run_again = FALSE;
  info->options_enabled = TRUE;
  info->pressed_cell = info->entered_cell = NULL;
  info->window_name = g_string_new (default_window_name);
}


void load_settings (world_info *info)
{
  const gchar *error;
  
  error = find_conf (info);
  if (error)
  {
    g_print ("error loading configuration: %s\n", error);
    set_window (info);
  }
  else
  {
    parse_config (info, pm_settings);
    set_window (info);
    parse_config (info, pm_programs);
  }
  info->x_icons_new = info->x_icons;
  info->y_icons_new = info->y_icons;
  if (info->tab_count == 0)
    add_tab (info, "Applications");
}

