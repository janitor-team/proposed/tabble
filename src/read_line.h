
#include <stdio.h>

#include <gtk/gtk.h>

#include "world.h"

typedef struct
{
  GString *filename;
  gint pos, max_length;
  gchar *buffer;
  FILE *file;
} readline;


gboolean text_here (const GString *text);
void free_g_string (GString *text);
int char_find (const gchar* text, char sought);
const gchar* find_parent_dir (const gchar *source);
const gchar* find_parent_dir_slash (const gchar *source);
const gchar* find_filename (const gchar *source);
gboolean file_readable (const gchar *filename);
readline* read_start (const gchar *filename, gint max_length);
gboolean read_next (readline *context, GString **destination);
const gchar* cmdline_option (world_info *info, const gchar *option);
