
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkkeysyms.h>

#include "world.h"
#include "load_lists.h"
#include "add_stuff.h"
#include "edits.h"
#include "options.h"
#include "signals.h"
#include "icon.h"
#include "save_lists.h"
#include "read_line.h"


void world_hide (GtkWidget *widget, world_info *info)
{
  info->last_tab = gtk_notebook_get_current_page (GTK_NOTEBOOK (info->tabs));
  gtk_widget_hide_all (GTK_WIDGET (info->main_window));
}


void show_edit_buttons (world_info *info, gboolean show_buttons)
{
  cell_struct *cell;
  gint t, x, y;

  for (t = 0; t < info->tab_count; t++)
    for (y = 0; y < info->y_icons; y++)
      for (x = 0; x < info->x_icons; x++)
      {
        cell = get_grid (info, t, x, y);
        if (cell->filled == FALSE)
        {
          if (show_buttons)
            push_blank_cell (info, t, x, y);
          else
            show_empty_cell (info, t, x, y);
        }
        else
        {
         if (cell->button)
         {
          if (show_buttons)
           gtk_button_set_relief (GTK_BUTTON (cell->button), GTK_RELIEF_NORMAL);
          else
           gtk_button_set_relief (GTK_BUTTON (cell->button), GTK_RELIEF_NONE);
         } 
        }
      }
  info->blanks_shown = show_buttons;
}


gboolean key_event (GtkWidget *widget, GdkEventKey *event, world_info *info)
{
  gboolean key = FALSE;

  if ((event->keyval == GDK_Escape) && (event->type == GDK_KEY_RELEASE))
  {
    world_hide (widget, info);
    return FALSE;
  }
  
  if ((event->keyval == GDK_Shift_L) || (event->keyval == GDK_Shift_R))
    key = TRUE;
  if (!key)
    return FALSE;
  
  key = event->type == GDK_KEY_PRESS ? TRUE : FALSE;
  if (key == info->blanks_shown)
    return FALSE;

  show_edit_buttons (info, key);
  return FALSE;
}


void options (GtkWidget *widget, world_info *info)
{
  show_edit_buttons (info, FALSE);
  options_dialog (info);
}


void my_chdir (const gchar *dir)
{
  if (dir && (strcmp (dir, "") != 0))
  {
    if (chdir (dir))
      g_print ("invalid directory: %s\n", dir);
  }
}


int run_the_program (cell_struct *cell)
{
  world_info *info;
  GString *string;
  int sys_result;

  info = (world_info*) cell->info;

  my_chdir (cell->item->dir->str);
  string = g_string_new (NULL);
  g_string_sprintf (string, "%s &", cell->item->cmd->str);
  sys_result = system (string->str);
  my_chdir (info->pwd_dir->str);
  if (cell->icon_button)
    gtk_button_set_relief (GTK_BUTTON (cell->icon_button), GTK_RELIEF_NONE);
  return sys_result;
}


void popup_edits (GtkWidget *widget, cell_struct *cell)
{
  show_edit_buttons ((world_info*) cell->info, FALSE);
  edits_dialog (cell);
}


void popup_select (GtkWidget *widget, cell_struct *cell)
{
  world_info *info;
  
  info = (world_info*) cell->info;
  show_edit_buttons (info, FALSE);
  info->selected_cell = cell;
}


void popup_copy (GtkWidget *widget, cell_struct *cell)
{
  world_info *info;
  
  info = (world_info*) cell->info;
  show_edit_buttons ((world_info*) cell->info, FALSE);
  copy_item (info->selected_cell->item, cell->item);
  pinup_item (cell);
  save_all (info);
}


void popup_delete (GtkWidget *widget, cell_struct *cell)
{
  world_info *info;
  
  info = (world_info*) cell->info;
  g_string_assign (cell->item->name, "");
  g_string_assign (cell->item->cmd, "");
  g_string_assign (cell->item->icon, "");
  cell->filled = FALSE;
  show_edit_buttons (info, FALSE);
  info->selected_cell = NULL;
  save_all (info);
}


void right_click_button (GdkEventButton *event, cell_struct *cell)
{
  GtkWidget *menu, *menuitem;
  world_info *info;

  info = (world_info*) cell->info;
  show_edit_buttons (info, FALSE);

  menu = gtk_menu_new();
  menuitem = gtk_menu_item_new_with_label ("Edit");
  g_signal_connect (menuitem, "activate", G_CALLBACK (popup_edits), cell);
  gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
  if (cell->filled)
  {
    menuitem = gtk_menu_item_new_with_label ("Copy this source");
    g_signal_connect (menuitem, "activate", G_CALLBACK (popup_select), cell);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
  }
  if (info->selected_cell && cell != info->selected_cell)
  {
    menuitem = gtk_menu_item_new_with_label ("Copy here");
    g_signal_connect (menuitem, "activate", G_CALLBACK (popup_copy), cell);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
  }
  if (cell->filled)
  {
    menuitem = gtk_menu_item_new_with_label ("Delete");
    g_signal_connect (menuitem, "activate", G_CALLBACK (popup_delete), cell);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
  }
  gtk_widget_show_all (menu);
  gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL,
    (event != NULL) ? event->button : 0,
    gdk_event_get_time ((GdkEvent*) event));
}


gboolean click_enter (GtkWidget *widget, GdkEventButton *event,
  cell_struct *cell)
{
  world_info *info;

  info = (world_info*) cell->info;
  info->entered_cell = cell;
  return FALSE;
}


gboolean click_leave (GtkWidget *widget, GdkEventButton *event,
  cell_struct *cell)
{
  world_info *info;

  info = (world_info*) cell->info;
  info->entered_cell = NULL;
  return FALSE;
}


gboolean click_press (GtkWidget *widget, GdkEventButton *event,
  cell_struct *cell)
{
  world_info *info;

  info = (world_info*) cell->info;
  if (event->button > 3)
    return TRUE;
  if (!info->options_enabled && event->button == 3)
    return TRUE;
  if (!cell->filled && event->button != 3)
    return TRUE;

  info->pressed_cell = info->entered_cell = cell;
  gtk_button_pressed (GTK_BUTTON (cell->button));
  return FALSE;
}


gboolean click_release (GtkWidget *widget, GdkEventButton *event,
  cell_struct *cell)
{
  world_info *info;

  info = (world_info*) cell->info;
  if (info->pressed_cell && (info->pressed_cell = info->entered_cell))
  {
    gtk_button_released (GTK_BUTTON (info->pressed_cell->button));
    switch (event->button)
    {
      case 1:
        if (cell->filled)
          run_the_program (cell);
        break;

      case 2:
        if (cell->filled)
        {
          world_hide (NULL, info);
          run_the_program (cell);
        }
        break;

      case 3:
        right_click_button (event, cell);
        break;
    }
  }
  info->pressed_cell = info->entered_cell = NULL;
  return FALSE;
}


gboolean key_activate (GtkWidget *widget, cell_struct *cell)
{
  world_info *info;

  info = (world_info*) cell->info;
  world_hide (NULL, info);
  run_the_program (cell);
  return FALSE;
}


void choices (world_info *info)
{
  GtkWidget *vbox, *hbox, *button;

  g_signal_connect (G_OBJECT (info->main_window), "destroy",
    G_CALLBACK (gtk_main_quit), info);
  gtk_container_set_border_width (GTK_CONTAINER (info->main_window),
   info->frame_spacing);

  gtk_window_set_default_icon (gdk_pixbuf_new_from_xpm_data (tabble_icon()));

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (info->main_window), vbox);
  
  info->tabs = gtk_notebook_new();
  gtk_notebook_set_tab_pos (GTK_NOTEBOOK (info->tabs), GTK_POS_TOP);
  gtk_notebook_set_scrollable (GTK_NOTEBOOK (info->tabs), TRUE);
  
  load_settings (info);
  
  gtk_box_pack_start (GTK_BOX (vbox), info->tabs, FALSE, FALSE, 0);

  hbox = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, info->frame_spacing);

  if (info->options_enabled)
  {
    button = gtk_button_new_with_label (" Options ");
    gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
    g_signal_connect (G_OBJECT (button), "clicked",
      G_CALLBACK (options), info);
  }

  button = gtk_button_new_with_label (" Hide ");
  gtk_box_pack_end (GTK_BOX (hbox), button, FALSE, FALSE, 0);
  g_signal_connect (G_OBJECT (button), "clicked",
    G_CALLBACK (world_hide), info);

  signal_init (info);
  
  if (info->options_enabled)
  {
    g_signal_connect (G_OBJECT (info->main_window), "key-press-event",
      G_CALLBACK (key_event), info);
    g_signal_connect (G_OBJECT (info->main_window), "key-release-event",
      G_CALLBACK (key_event), info);
  }

  gtk_widget_show_all (info->main_window);
  show_edit_buttons (info, FALSE);
}

