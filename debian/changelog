tabble (0.45-2) unstable; urgency=medium

  * QA upload.
  * Upload to unstable.
  * debian/copyright: changed my email in packaging block.

 -- Fabio Augusto De Muzio Tobich <ftobich@debian.org>  Mon, 16 Aug 2021 11:49:23 -0300

tabble (0.45-1) experimental; urgency=medium

  * QA upload.
  * New upstream release.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Added Rules-Requires-Root: no.
      - Bumped Standards-Version to 4.5.1.
      - Removed autotools-dev from Build-Depends, not necessary anymore.
      - Updated Homepage field in source stanza to use a secure URI.
      - Updated Vcs-* fields in source stanza.
  * debian/copyright:
      - Added myself and Gregor Herrmann at the packaging block.
      - Updated Format field to use a secure URI.
      - Updated Source field to use a secure URI.
  * debian/patches/010_fix-spelling-errors.patch: added.
  * debian/rules:
      - Added -D_FORTIFY_SOURCE flag for hardening.
      - Added override_dh_installman.
      - Removed man block, override_dh_auto_build and man target.
      - Removed unnecessary DEB_LDFLAGS_MAINT_APPEND.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/watch:
      - Bumped to version 4.
      - Updated URI to fix a 'not found' error when downloading new version.

 -- Fabio Augusto De Muzio Tobich <ftobich@gmail.com>  Tue, 16 Mar 2021 22:47:14 -0300

tabble (0.43-3) unstable; urgency=medium

  * QA upload.
    Set Maintainer to "Debian QA Group <packages@qa.debian.org>".
    Cf. #778221
  * Fix "ftbfs with GCC-5": apply patch from David S. Roth:
    build with "-std=gnu89".
    (Closes: #778137)
  * Fix "please make the build reproducible": apply patch from Reiner
    Herrmann: use last changelog date as a timestamp.
    (Closes: #782222)

 -- gregor herrmann <gregoa@debian.org>  Sun, 19 Jul 2015 16:07:26 +0200

tabble (0.43-2) unstable; urgency=low

  * debian/tabble.1.pod, tabble-wrapper.1.pod
    - Correct pod tags and OPTIONS section (Closes: #665214).
  * debian/tabble-wrapper
    - Pass command line arguments to tabble(1). Patch thanks to Justin B
      Rye <jbr@edlug.org.uk> in Bug#665214.
  * debian/rules
    - Use hardened build flags (CPPFLAGS: -D_FORTIFY_SOURCE=2)
      http://wiki.debian.org/ReleaseGoals/SecurityHardeningBuildFlags

 -- Jari Aalto <jari.aalto@cante.net>  Sun, 16 Sep 2012 17:26:44 +0300

tabble (0.43-1) unstable; urgency=low

  * Initial release (Closes: #582889).

 -- Jari Aalto <jari.aalto@cante.net>  Thu, 08 Mar 2012 23:06:41 -0500
